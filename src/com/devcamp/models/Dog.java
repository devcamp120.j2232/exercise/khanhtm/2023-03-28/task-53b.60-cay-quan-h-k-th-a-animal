package com.devcamp.models;

public class Dog extends Mammal {
 public Dog(String name){
  super(name);
 }
 public void greets(){
  System.out.println("Gauuuu");
 }

 public void greets(Dog Dog){
  System.out.println("Gauuuuuuuuu");
 }

 @Override
 public String toString() {
  return String.format("Dog[Mammal[Animal [Name = %s]]]", name);
 }
}