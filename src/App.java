import com.devcamp.models.Animal;
import com.devcamp.models.Cat;
import com.devcamp.models.Dog;
import com.devcamp.models.Mammal;

public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Poodle Toy");
        System.out.println("Animal1:");
        System.out.println(animal1);

        Animal animal2 = new Animal("Pug Blue");
        System.out.println("Animal2:");
        System.out.println(animal2);

        Mammal mammal1 = new Mammal("Toy Boiz");
        System.out.println("Mammal1:");
        System.out.println(mammal1);

        Mammal mammal2 = new Mammal("Pu Pu");
        System.out.println("Mammal2:");
        System.out.println(mammal2);

        Cat cat1 = new Cat("Poodle");
        System.out.println("Cat1:");
        System.out.println(cat1);

        Cat cat2 = new Cat("Toy");
        System.out.println("Cat2:");
        System.out.println(cat2);

        Dog dog1 = new Dog("Lap Xuong");
        System.out.println("Dog1:");
        System.out.println(dog1);

        Dog dog2 = new Dog("Bull");
        System.out.println("Dog2:");
        System.out.println(dog2);

        cat1.greets();
        cat2.greets();

        dog1.greets();
        dog2.greets();

        dog1.greets(dog2);
        dog2.greets(dog1);
    }
}
